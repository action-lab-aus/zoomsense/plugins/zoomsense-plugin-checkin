# Bicara Pluign - Checkin

Bicara Checkin Plugin allows coaches to walk through the TOPS session checkin questions with parents.

## Local Development

Once the `.runtimeconfig.json` file is prepared under the `/functions` folder (configuration details can be found under [Bicara Core Step 6](https://gitlab.com/action-lab-aus/bicara/zoomsense-tops/-/blob/main/docs/7_QuickStart_Functions.md)), Firebase Functions can be tested locally using the command:

```
npm run emulators
```

This ensures that the emulator data will be imported when Firebase Emulator starts and exported when the session ends.

Under `app/vue.config.js`, we specifically exclude Firebase-related dependencies from the output bundles. To run the plugin app locally, please comment on the following line from the file:

```
configureWebpack: {
    // externals: [/^firebase\/.+$/, /^vuefire\/.+$/],
},
```
