const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const db = admin.database();

/**
 * When the checkin plugin is enabled for the current section, check whether any responses
 * have bee stored under the `/data/topsCheckin/` node and load the responses to the current
 * plugin config node if needed
 *
 * @param {String} meetingId Session ID
 */
exports.onCheckinEnabled = functions.database
  .ref('/config/{meetingId}/current/currentState/plugins/checkin/enabled')
  .onCreate(async (snapshot, context) => {
    const meetingId = context.params.meetingId;

    try {
      // Get the responses recorded under the `/data/topsCheckin` node
      const responses = (await db.ref(`data/topsCheckin/${meetingId}`).once('value')).val();
      // functions.logger.info('responses: ', responses);

      if (responses) {
        const responsesArr = [];
        responses.forEach(res => {
          responsesArr.push(res.response);
        });

        // Set the responses to the current plugin node
        await db.ref(`config/${meetingId}/current/currentState/plugins/checkin/responses`).set(responsesArr);
      }
    } catch (e) {
      functions.logger.error(`Checkin-onCheckinEnabled Error: `, e);
    }
  });
